// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "school_gameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SCHOOL_GAME_API Aschool_gameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
